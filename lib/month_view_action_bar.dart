import 'package:calendar_todo/sxwnl/cust_date.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MonthViewActionBar extends StatelessWidget {
  final double screenWidth;
  final DateTime showMonth;
  final Function(DateTime month) onDateChangeFn;
  final String Function() getFestivalText;
  final String Function(String) onSaveFn;
  MonthViewActionBar({
    this.screenWidth,
    this.showMonth,
    this.onDateChangeFn,
    this.getFestivalText,
    this.onSaveFn,
  });

  ///
  @override
  Widget build(BuildContext context) {
    var lineTitle = <Widget>[];
    CustomDate ct = CustomDate(showMonth);

    ///
    lineTitle.add(Container(
      // width: screenWidth * 7 / 10,
      // alignment: Alignment.center,
      //padding: EdgeInsets.fromLTRB(screenWidth / 100, 0, screenWidth / 100, 0),
      // color: Colors.lightBlueAccent,
      child: TextButton(
        // color: Colors.lightBlueAccent,
        ///显示阳历年 月 日
        child: Text(
          "${showMonth.year}年" +
              // ((showMonth.month < 10) ? "" : "") +
              "${showMonth.month}月 " +
              //  ((showMonth.day < 10) ? "" : "") +
              "${showMonth.day}日",
          style: TextStyle(fontSize: screenWidth / 20, color: Colors.black),
        ),
        onPressed: () async {
          await showCupertinoModalPopup(
            context: context,
            builder: (context) {
              return Container(
                // width: screenWidth,
                height: 300,
                // color: Colors.red,
                child: CupertinoDatePicker(
                  mode: CupertinoDatePickerMode.date,
                  onDateTimeChanged: (DateTime pickDate) {
                    if (null != pickDate) {
                      onDateChangeFn(pickDate);
                    }
                  },
                  initialDateTime: showMonth,
                  minimumDate: DateTime(1600),
                  maximumDate: DateTime(9000),
                ),
              );
            },
          );
        },
      ),
    ));

    ///干支年 月 日
    lineTitle.add(Row(
      children: [
        SizedBox(width: screenWidth * 3 / 6),
        Text("${ct.gzYear.substring(0, 1)}" + "\n${ct.gzYear.substring(1, 2)}",
            style: TextStyle(fontSize: screenWidth / 20, color: Colors.black)),
        Text("${ct.gzMonth.substring(0, 1)}" + "\n${ct.gzMonth.substring(1, 2)}",
            style: TextStyle(fontSize: screenWidth / 20, color: Colors.black)),
        Text("${ct.gzDay.substring(0, 1)}" + "\n${ct.gzDay.substring(1, 2)}",
            style: TextStyle(fontSize: screenWidth / 20, color: Colors.red)),
        Text("${ct.gzHour.substring(0, 1)}" + "\n${ct.gzHour.substring(1, 2)}",
            style: TextStyle(fontSize: screenWidth / 20, color: Colors.black)),
      ],
    ));

    ///返回今日
    lineTitle.add(
      Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          SizedBox(width: screenWidth * 4 / 5),
          GestureDetector(
            onTap: () {
              onDateChangeFn(DateTime.now());
            },
            child: Container(
              width: screenWidth / 8,
              height: screenWidth / 8,
              decoration: BoxDecoration(
                // color: Colors.yellowAccent,
                border: Border.all(width: 2.0, color: Colors.black38),
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              child: FittedBox(
                child: Text(
                  "返回\n今日",
                  style: TextStyle(color: Colors.red, fontSize: screenWidth / 15, decoration: TextDecoration.none),
                ),
              ),
            ),
          ),
        ],
      ),
    );

    var lineAction = <Widget>[];

    ///上一年
    lineAction.add(
      Container(
        child: TextButton(
          //  color: Colors.orange,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(child: Icon(Icons.arrow_back_ios)),
              Text("上一年", style: TextStyle(fontSize: screenWidth / 25)),
            ],
          ),
          onPressed: () {
            final lastMonth = DateTime(showMonth.year - 1, showMonth.month, 1);
            onDateChangeFn(lastMonth);
          },
        ),
      ),
    );
    lineAction.add(SizedBox(width: screenWidth / 500));

    ///下一年
    lineAction.add(
      Container(
        child: TextButton(
          //color: Colors.orange,
          child: Row(mainAxisSize: MainAxisSize.min, children: [
            Text("下一年", style: TextStyle(fontSize: screenWidth / 25)),
            Icon(Icons.arrow_forward_ios),
          ]),
          onPressed: () {
            final nextMonth = DateTime(showMonth.year + 1, showMonth.month, 1);
            onDateChangeFn(nextMonth);
          },
        ),
      ),
    );
    lineAction.add(SizedBox(width: screenWidth / 50));

    ///上一月
    lineAction.add(
      Container(
        //color: Colors.orange,
        child: TextButton(
          // color: Colors.cyan,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(child: Icon(Icons.arrow_back_ios)),
              Text("上一月", style: TextStyle(fontSize: screenWidth / 25)),
            ],
          ),
          onPressed: () {
            var lastMonth = DateTime(showMonth.year, showMonth.month - 1, showMonth.day);
            if (lastMonth.day != showMonth.day) {
              lastMonth = DateTime(showMonth.year, showMonth.month, 0);
            }

            onDateChangeFn(lastMonth);
          },
        ),
      ),
    );
    lineAction.add(SizedBox(width: screenWidth / 500));

    ///下一月
    lineAction.add(
      Container(
        // color: Colors.orange,
        child: TextButton(
          // color: Colors.cyan,
          child: Row(mainAxisSize: MainAxisSize.min, children: [
            Text("下一月", style: TextStyle(fontSize: screenWidth / 25)),
            Icon(Icons.arrow_forward_ios),
          ]),
          onPressed: () {
            var nextMonth = DateTime(showMonth.year, showMonth.month + 1, showMonth.day);
            if (nextMonth.day != showMonth.day) {
              nextMonth = DateTime(showMonth.year, showMonth.month + 2, 0);
            }
            onDateChangeFn(nextMonth);
          },
        ),
      ),
    );

    return FittedBox(
      child: Container(
        width: screenWidth * 8 / 9,
        margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        decoration: BoxDecoration(
          //color: Colors.redAccent,
          border: Border.all(width: 0.5, color: Colors.black38),
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
        child: FittedBox(
            child: Column(
          children: [
            Container(
              child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: lineAction),
            ),
            Stack(
              //alignment: AlignmentDirectional.center,
              alignment: AlignmentDirectional.centerStart,
              children: lineTitle,
            ),
          ],
        )),
      ),
    );
  }
}
