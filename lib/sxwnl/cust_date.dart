import 'sxwnl_JD.dart';
// import 'sxwnl_Lunar.dart';
import 'sxwnl_LunarHelper.dart';
// import 'sxwnl_SSQ.dart';
import 'sxwnl_XL.dart';
import 'sxwnl_obb.dart';
import 'sxwnl_tool.dart';

class CustomDate {
  //阳历
  DateTime date;
  int year;
  int month;
  int day;
  int hour;
  int minute;
  double jde;
  //干支
  String gzYear;
  String gzMonth;
  String gzDay;
  String gzHour;
  // String jianChu;
  // String riQin;
  // String riQinFullName;
  // String huangHei;
  String xingZuo; //星座
  // String yueJiangName; //月将名称
  // String yueJiangZhi; //月将地支

  //阴历
  // String moons; //阴历月日
  // int moonMonth; //阴历月数字
  // int moonDay; //阴历日数字

// 计算指定日期的干支，节气，日禽，建除，黄黑,阴历等 [新加]
  CustomDate(DateTime dt, [double jw]) {
    jw ??= 121.466667; //默认上海经度
    date = dt;
    year = dt.year;
    month = dt.month;
    day = dt.day;
    hour = dt.hour;
    minute = dt.minute;
    //
    mlCalc(dt, jw);
    // jianChu = getRi12Jian(gzMonth.substring(1, 2), gzDay.substring(1, 2));
    // riQin = getRiQin(dt.weekday, gzDay);
    // huangHei = getHuangHei(gzMonth, gzDay);
    //
    // moon(dt.year, dt.month, dt.day);
  }
  static double pi2 = Math.PI * 2;

  /// 命理八字计算 jw 经度(可选值，默认上海经度)
  void mlCalc(DateTime dt, double jw) {
    // print("mlcalc    ----> ${dt.toString()}");
    double tx = time2Hour(dt.hour, dt.minute, dt.second);
    int yx = LunarHelper.year2Ayear(dt.year);
    double jd = JD.JD__(yx.toDouble(), dt.month.toDouble(), dt.day.toDouble() + tx / 24);
    jde = jd;
    int curTZ = -8; //中国时区
    double jdx = jd + curTZ / 24 - LunarHelper.J2000;
    //jw ??= 121.466667; //默认上海经度
    double radd = 180 / Math.PI;
    // print("mlcalc ---> jdx $jdx jw: $jw");
    mingLiBaZi(jdx, jw / radd);
  }

  /// 八字计算
  void mingLiBaZi(double jd, double jwr) {
    // print("mlbz ---> jd: $jd");
    double jd2 = jd + dtt(jd);
    // print("mlbz---> jd2: $jd2");
    double w = XL.S_aLon(jd2 / 36525, -1);
    // print("mlbz ---> w: $w");
    int k = JD.int2((w / pi2 * 360 + 45 + 15 * 360) / 30);
    // print("mlbz ---> k : $k");
    // print("-------------------------");
    jd += ptyZty2(jd2) + jwr / Math.PI / 2;
    // print("mlbz ---jd+pty: ${jd += ptyZty2(jd2)}");
    // print("mlbz ---> j/math/pi: ${jwr / Math.PI / 2}");
    // print("mlbz ---> jd+= $jd");
    jd += 13 / 24;
    // print("mlbz ---> jd+=13/24: $jd");

    int D = jd.floor();
    // print("mlbz ===> jd: $jd D: $D jd-D=${jd - D}");
    int SC = JD.int2((jd - D) * 12);
    // print("mlbz ---> D: $D SC: $SC");
    //
    int v = JD.int2(k / 12 + 6000000);
    String niangz = OBB.Gan[v % 10] + OBB.Zhi[v % 12]; //年干支
    // print("mlbz ---> v: $v ygz: $niangz");

    v = k + 2 + 60000000;
    String yuegz = OBB.Gan[v % 10] + OBB.Zhi[v % 12];
    // print("mlbz ---> v: $v mgz: $yuegz");

    v = D - 6 + 9000000;
    String rigz = OBB.Gan[v % 10] + OBB.Zhi[v % 12];
    // print("mlbz ---> v: $v, dgz: $rigz");

    v = (D - 1) * 12 + 90000000 + SC;
    String shigz = OBB.Gan[v % 10] + OBB.Zhi[v % 12];
    // print("mlbz ---> v: $v,hgz: $shigz");
    // print(" $niangz$yuegz$rigz$shigz");

    gzYear = niangz;
    gzMonth = yuegz;
    gzDay = rigz;
    gzHour = shigz;
    // print(" mingLiBaZi----> $gzYear $gzMonth $gzDay $gzHour");

    ///
    v -= SC;
    for (int i = 0; i < 13; i++) {
      String c = OBB.Gan[(v + i) % 10] + OBB.Zhi[(v + i) % 12];
      // print("$i $c");
    }
  }

  /// 计算本月所有日期的日十二建信息
  // String getRi12Jian(String yueJian, String riZhi) {
  //   String result = "";
  //   int posYueJian = -1, posRiZhi = -1, pos;

  //   for (int i = 0; i < OBB.Zhi.length; i++) {
  //     if (OBB.Zhi[i] == yueJian) {
  //       posYueJian = i;
  //       break;
  //     }
  //   }
  //   if (posYueJian >= 0) {
  //     for (int i = posYueJian + 1; i < OBB.DoubleRiJian12.length; i++) {
  //       if (OBB.DoubleRiJian12[i] == riZhi) {
  //         posRiZhi = i;
  //         break;
  //       }
  //     }

  //     if (posRiZhi >= posYueJian) {
  //       pos = posRiZhi - posYueJian;
  //       result = OBB.DoubleRiJian12 [pos];
  //     }
  //   }
  //   return result;
  // }

  /// 日禽 [新加]
  /// 周几 日干支
  // String getRiQin(int weekday, String dgz) {
  //   int wn = 0;
  //   switch (dgz.substring(1, 2)) {
  //     case "亥":
  //       var w0 = 17;
  //       wn = w0 + 7 * weekday + weekday;
  //       wn > 28 ? wn = wn % 28 : null;
  //       break;
  //     case "卯":
  //       var w0 = 17;
  //       wn = w0 + 7 * weekday + weekday;
  //       wn > 28 ? wn = wn % 28 : null;
  //       break;
  //     case "未":
  //       var w0 = 17;
  //       wn = w0 + 7 * weekday + weekday;
  //       wn > 28 ? wn = wn % 28 : null;
  //       break;
  //     case "巳":
  //       var w0 = 3;
  //       wn = w0 + 7 * weekday + weekday;
  //       wn > 28 ? wn = wn % 28 : null;
  //       break;
  //     case "酉":
  //       var w0 = 3;
  //       wn = w0 + 7 * weekday + weekday;
  //       wn > 28 ? wn = wn % 28 : null;
  //       break;
  //     case "丑":
  //       var w0 = 3;
  //       wn = w0 + 7 * weekday + weekday;
  //       wn > 28 ? wn = wn % 28 : null;
  //       break;
  //     case "寅":
  //       var w0 = 24;
  //       wn = w0 + 7 * weekday + weekday;
  //       wn > 28 ? wn = wn % 28 : null;
  //       break;
  //     case "午":
  //       var w0 = 24;
  //       wn = w0 + 7 * weekday + weekday;
  //       wn > 28 ? wn = wn % 28 : null;
  //       break;
  //     case "戌":
  //       var w0 = 24;
  //       wn = w0 + 7 * weekday + weekday;
  //       wn > 28 ? wn = wn % 28 : null;
  //       break;
  //     case "申":
  //       var w0 = 10;
  //       wn = w0 + 7 * weekday + weekday;
  //       wn > 28 ? wn = wn % 28 : null;
  //       break;
  //     case "子":
  //       var w0 = 10;
  //       wn = w0 + 7 * weekday + weekday;
  //       wn > 28 ? wn = wn % 28 : null;
  //       break;
  //     case "辰":
  //       var w0 = 10;
  //       wn = w0 + 7 * weekday + weekday;
  //       wn > 28 ? wn = wn % 28 : null;
  //       break;
  //   }
  //   riQinFullName = OBB.starName[wn];
  //   return OBB.starName[wn].substring(0, 1);
  // }

  /// 阳历纪年 2年的节气
  // jieQiYearCalc(int year) {
  //   LunarHelper.getNianLi(year);
  //   List<DateTime> jqt = LunarHelper.jq24t;
  //   LunarHelper.getNianLi(year + 1);
  //   jieQiYear = jqt;
  // }

  /// 阴历月日 阳历星座 月将
  // void moon(int y, int m, int dayCount) {
  //   // 日历物件初始化
  //   JD.h = 12;
  //   JD.m = 0;
  //   JD.s = 0.1;
  //   JD.Y = y;
  //   JD.M = m;
  //   JD.D = 1;

  //   double bd0 = Math.Floor2Double(JD.toJD()) - LunarHelper.J2000; // 公历某年的月首,中午
  //   JD.M++;
  //   // 如果月份大于 12, 则年数 + 1, 月数取 1
  //   if (JD.M > 12) {
  //     JD.Y++;
  //     JD.M = 1;
  //   }

  //   DayInfo day = DayInfo();
  //   day.d0 = bd0 + dayCount; // 儒略日,北京时12:00

  //   // 农历月历 如果d0已在计算农历范围内则不再计算
  //   if ((SSQ.ZQ.Count == 0) || (day.d0 < SSQ.ZQ[0]) || (day.d0 >= SSQ.ZQ[24])) {
  //     SSQ.calcY(day.d0);
  //   }
  //   // 农历所在月的序数
  //   int mk = Math.Floor2Double((day.d0 - SSQ.HS[0]) / 30).toInt();
  //   if (mk < 13 && SSQ.HS[mk + 1] <= day.d0) {
  //     mk++;
  //   }

  //   day.lunarDay = (day.d0 - SSQ.HS[mk]).toInt() + 1; // 农历日，由距农历月首的编移量+1,1对应初一
  //   day.lunarDayName = OBB.rmc[day.lunarDay - 1]; // 农历日名称

  //   // 节气相关的内容
  //   // day.cur_dz = day.d0 - SSQ.ZQ[0]; // 距冬至的天数
  //   // day.cur_xz = day.d0 - SSQ.ZQ[12]; // 距夏至的天数
  //   // day.cur_lq = day.d0 - SSQ.ZQ[15]; // 距立秋的天数
  //   // day.cur_mz = day.d0 - SSQ.ZQ[11]; // 距芒种的天数
  //   // day.cur_xs = day.d0 - SSQ.ZQ[13]; // 距小暑的天数

  //   // 月的信息
  //   // if ((day.d0 == SSQ.HS[mk]) || (day.d0 == bd0)) {
  //   //   int index = mk - 1;
  //   //   index < 0 ? index += 12 : index;
  //   //   if (index == 0) {
  //   //     //2022-12-22 00:00:00.000
  //   //     day.lunarMonthName = "${SSQ.ym[13]}月"; // 月名称
  //   //     moonMonth = han2Int[SSQ.ym[13]]; //月数字
  //   //     moonDay = (day.d0 - SSQ.HS[0]).toInt(); // 农历日数字，由距农历月首的编移量+1,1对应初一
  //   //   } else {
  //   //     day.lunarMonthName = "${SSQ.ym[index]}月"; // 月名称
  //   //     moonMonth = han2Int[SSQ.ym[index]]; //月数字
  //   //     moonDay = (day.d0 - SSQ.HS[index]).toInt(); // 农历日数字，由距农历月首的编移量+1,1对应初一
  //   //   }
  //   //   day.lunarDayName = OBB.rmc[moonDay - 1]; // 农历日名称
  //   //   moons = "${day.lunarMonthName}${day.lunarDayName}";
  //   //   // day.Ldn = SSQ.dx[mk]; // 月大小
  //   //   // day.lunarMonthDayCount = day.Ldn.toInt();
  //   //   // day.lunarRunyue = (SSQ.leap != 0 && SSQ.leap == mk) ? "闰" : ""; // 闰状况
  //   // } else {
  //   //   DayInfo day2 = DayInfo();
  //   //   day2.d0 = bd0 + dayCount - 1;
  //   //   day.lunarMonthIndex = (mk + 12 - 2) % 12;
  //   //   day.lunarMonthName = "${SSQ.ym[mk]}月"; // 月名称
  //   //   moonMonth = han2Int[SSQ.ym[mk]]; //月数字
  //   //   //
  //   //   moonDay = (day.d0 - SSQ.HS[mk]).toInt(); // 农历日数字
  //   //   day.lunarDayName = OBB.rmc[moonDay - 1]; // 农历日名称
  //   //   moons = "${day.lunarMonthName}${day.lunarDayName}";
  //   // }

  //   // 星座  mk:星座所在月的序数
  //   // mk = Math.Floor2Double((day.d0 - SSQ.ZQ[0] - 15) / 30.43685).toInt();
  //   // // int mkx = JD.int2((day.d0 - SSQ.ZQ[0] - 15) / 30.43685);
  //   // // print("mk: $mk mkx: $mkx");
  //   // if (mk < 11 && day.d0 >= SSQ.ZQ[2 * mk + 2]) {
  //   //   mk++;
  //   // }
  //   // day.xingzuo = "${OBB.XiZ[((mk + 12) % 12).toInt()]}座";
  //   // xingZuo = day.xingzuo;
  //   //月将
  //   // yueJiangZhi = OBB.yjZhis[((mk + 12) % 12).toInt()];
  //   // yueJiangName = OBB.yjNames[((mk + 12) % 12).toInt()];
  // }

  /// 黄黑
  // String getHuangHei(String gz, String subgz) {
  //   String zs = gz.substring(1, 2);
  //   String subzs = subgz.substring(1, 2);
  //   String hhs = '';
  //   hhmap.forEach((key, value) {
  //     if (key == zs) {
  //       for (int i = 0; i < value.length; i++) {
  //         if (subzs == value[i]) {
  //           hhs = listhh[i];
  //           break;
  //         }
  //       }
  //       return;
  //     }
  //   });
  //   return hhs;
  // }

  // //黄黑
  // final List<String> listhh = ["青龙", "金匮", "司命", "明堂", "天德", "玉堂", "天刑", "白虎", "天牢", "朱雀", "玄武", "勾陈"];
  // //黄黑map
  // final Map<String, List<String>> hhmap = {
  //   //k:月建 v:对应arr 如正月建寅 子日青龙 亥日勾陈
  //   //k:日干支 v:时辰数组 如寅日 子时青龙 亥时勾陈
  //   "寅": ["子", "辰", "戌", "丑", "巳", "未", "寅", "午", "申", "卯", "酉", "亥"],
  //   "卯": ["寅", "午", "子", "卯", "未", "酉", "辰", "申", "戌", "巳", "亥", "丑"],
  //   "辰": ["辰", "申", "寅", "巳", "酉", "亥", "午", "戌", "子", "未", "丑", "卯"],
  //   "巳": ["午", "戌", "辰", "未", "亥", "丑", "申", "子", "寅", "酉", "卯", "巳"],
  //   "午": ["申", "子", "午", "酉", "丑", "卯", "戌", "寅", "辰", "亥", "巳", "未"],
  //   "未": ["戌", "寅", "申", "亥", "卯", "巳", "子", "辰", "午", "丑", "未", "酉"],
  //   "申": ["子", "辰", "戌", "丑", "巳", "未", "寅", "午", "申", "卯", "酉", "亥"],
  //   "酉": ["寅", "午", "子", "卯", "未", "酉", "辰", "申", "戌", "巳", "亥", "丑"],
  //   "戌": ["辰", "申", "寅", "巳", "酉", "亥", "午", "戌", "子", "未", "丑", "卯"],
  //   "亥": ["午", "戌", "辰", "未", "亥", "丑", "申", "子", "寅", "酉", "卯", "巳"],
  //   "子": ["申", "子", "午", "酉", "丑", "卯", "戌", "寅", "辰", "亥", "巳", "未"],
  //   "丑": ["戌", "寅", "申", "亥", "卯", "巳", "子", "辰", "午", "丑", "未", "酉"],
  // };
  // final Map<String, int> han2Int = {
  //   "正": 1,
  //   "二": 2,
  //   "三": 3,
  //   "四": 4,
  //   "五": 5,
  //   "六": 6,
  //   "七": 7,
  //   "八": 8,
  //   "九": 9,
  //   "十": 10,
  //   "冬": 11,
  //   "腊": 12,
  // };

  // /// 太冲天马
  // static String taiChongTianMa(String yjz, String hgz) {
  //   String hz = hgz.substring(1, 2);
  //   List<String> zhis = ["子", "丑", "寅", "卯", "辰", "巳", "午", "未", "申", "酉", "戌", "亥"];
  //   List<String> yjarr = SORT.sortArr(yjz, zhis);
  //   List<String> hzarr = SORT.sortArr(hz, zhis);

  //   String str = '';
  //   for (int i = 0; i < yjarr.length; i++) {
  //     if (yjarr[i] == "卯") {
  //       str = hzarr[i];
  //       break;
  //     }
  //   }
  //   return "天马: $str";
  // }

  // /// 天三门 卯为太冲 酉为从魁 未为小吉
  // static Map<String, String> tianSanMen(String yjz, String hgz) {
  //   String hz = hgz.substring(1, 2);
  //   List<String> zhis = ["子", "丑", "寅", "卯", "辰", "巳", "午", "未", "申", "酉", "戌", "亥"];
  //   List<String> yjarr = SORT.sortArr(yjz, zhis);
  //   List<String> hzarr = SORT.sortArr(hz, zhis);

  //   Map<String, String> xmap = {};
  //   for (int i = 0; i < yjarr.length; i++) {
  //     if (yjarr[i] == "卯") {
  //       xmap["太冲"] = hzarr[i]; //卯为太冲
  //     }
  //     if (yjarr[i] == "酉") {
  //       xmap["从魁"] = hzarr[i]; //酉为从魁
  //     }
  //     if (yjarr[i] == "未") {
  //       xmap["小吉"] = hzarr[i]; //未为小吉
  //     }
  //   }
  //   return xmap;
  // }

  // /// 地四户(除 定 危 开) 建加于时支上顺排
  // static Map<String, String> diSiHu(String hgz) {
  //   List<String> jcs = ["建", "除", "满", "平", "定", "执", "破", "危", "成", "收", "开", "闭"]; //顺序固定
  //   List<String> zhis = ["子", "丑", "寅", "卯", "辰", "巳", "午", "未", "申", "酉", "戌", "亥"];

  //   String hz = hgz.substring(1, 2);
  //   List<String> hzhis = SORT.sortArr(hz, zhis);
  //   Map<String, String> xmap = {};
  //   for (int i = 0; i < jcs.length; i++) {
  //     if (jcs[i] == "除") {
  //       xmap["除"] = hzhis[i];
  //     }
  //     if (jcs[i] == "定") {
  //       xmap["定"] = hzhis[i];
  //     }
  //     if (jcs[i] == "危") {
  //       xmap["危"] = hzhis[i];
  //     }
  //     if (jcs[i] == "开") {
  //       xmap["开"] = hzhis[i];
  //     }
  //   }
  //   return xmap;
  // }

  /// ----------------------------------------------
  static double ptyZty2(double jd2) {
    return XL.shiCha2(jd2 / 36525);
  }

  //转换小时分钟秒为小时单位
  static double time2Hour(int hour, minute, second) {
    return hour + minute / 60 + second / 3600;
  }

  static double dtt(t) {
    return dtCalc(t / 365.2425 + 2000) / 86400.0;
  }

  static double dtCalc(y) {
    var y0 = dtAt[dtAt.length - 2];
    var t0 = dtAt[dtAt.length - 1];
    if (y >= y0) {
      var jsd = 31;
      if (y > y0 + 100) return dtExt(y, jsd);
      var v = dtExt(y, jsd);
      var dv = dtExt(y0, jsd) - t0;
      return v - dv * (y0 + 100 - y) / 100;
    }
    var i, d = dtAt;
    for (i = 0; i < d.length; i += 5) {
      if (y < d[i + 5]) break;
    }
    var t1 = (y - d[i]) / (d[i + 5] - d[i]) * 10, t2 = t1 * t1, t3 = t2 * t1;
    return d[i + 1] + d[i + 2] * t1 + d[i + 3] * t2 + d[i + 4] * t3;
  }

  static dtExt(y, jsd) {
    var dy = (y - 1820) / 100;
    return -20 + jsd * dy * dy;
  }

  static List dtAt = [
    -4000,
    108371.7,
    -13036.80,
    392.000,
    0.0000,
    -500,
    17201.0,
    -627.82,
    16.170,
    -0.3413,
    -150,
    12200.6,
    -346.41,
    5.403,
    -0.1593,
    150,
    9113.8,
    -328.13,
    -1.647,
    0.0377,
    500,
    5707.5,
    -391.41,
    0.915,
    0.3145,
    900,
    2203.4,
    -283.45,
    13.034,
    -0.1778,
    1300,
    490.1,
    -57.35,
    2.085,
    -0.0072,
    1600,
    120.0,
    -9.81,
    -1.532,
    0.1403,
    1700,
    10.2,
    -0.91,
    0.510,
    -0.0370,
    1800,
    13.4,
    -0.72,
    0.202,
    -0.0193,
    1830,
    7.8,
    -1.81,
    0.416,
    -0.0247,
    1860,
    8.3,
    -0.13,
    -0.406,
    0.0292,
    1880,
    -5.4,
    0.32,
    -0.183,
    0.0173,
    1900,
    -2.3,
    2.06,
    0.169,
    -0.0135,
    1920,
    21.2,
    1.69,
    -0.304,
    0.0167,
    1940,
    24.2,
    1.22,
    -0.064,
    0.0031,
    1960,
    33.2,
    0.51,
    0.231,
    -0.0109,
    1980,
    51.0,
    1.29,
    -0.026,
    0.0032,
    2000,
    63.87,
    0.1,
    0,
    0,
    2005,
    64.7,
    0.21,
    0,
    0,
    2012,
    66.8,
    0.22,
    0,
    0,
    2018,
    69.0,
    0.36,
    0,
    0,
    2028,
    72.6
  ];
}
