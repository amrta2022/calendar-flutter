#!/bin/bash

### app
version="1.3.6"
flutter build apk --obfuscate --split-debug-info=build --release --build-name=$version --build-number=8
sleep 6

### linux
# flutter build linux --release